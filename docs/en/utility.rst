=====================
Utility-based method
=====================

In a utility-based AI system, the behavior of an agent is determined by evaluating the expected utility of all actions that are available to the agent.