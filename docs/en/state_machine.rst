State Machine
================================

A state machine or finite state machine (*FSM*) is a widely used mathematical model in computer programs.
A state transits to another state when a corresponding input variable is received. 
In some cases, a state travels to itself. [#]_
  
.. figure:: ../images/fsm.drawio.png
    :name: fig_fsm

    Finite State Machine

FSM or hierarchical FSM was often used to describe a non-player character's behavior in early computer games.
It is replaced by Behavior Tree gradually due to its inflexibility. [#]_

.. rubric:: Footnotes

.. [#] https://en.wikipedia.org/wiki/Finite-state_machine

.. [#] FSM is still widely used to control character animations at present.