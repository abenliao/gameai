Backpropagation
================

In fitting a neural network, backpropagation computes the gradient of the loss function with respect to the weights of the network for a single input-output example, and update the weights to minimize loss by the chain rule, computing the gradient one layer at a time, iterating backward from the last layer to the first layer one by one.

Denote:

x: input (vector of features)

y: target output, for classification, output will be a vector of class probabilities (e.g., (0.2,.07,0.1)), and target output is a specific class, encoded by the one-hot/dummy variable (e.g., (0,1,0)).

C: loss function or cost function. For classification, this is usually cross entropy, while for regression it is usually squared error loss (SEL).

L: the number of layers

:math:`W^l = (w_{jk}^l)`: the weights between layer l-1 and l, where :math:`w_{jk}^l` is the weight between the k-th node in layer l-1 and the j-th node in layer l.

:math:`f^l`: activation functions at layer l. For classification the last layer is usually the logistic function for binary classification, and softmax for multi-class classification, where for the hidden layers this was traditionally a sigmoid function (logistic function or others) on each onde, but today is more varied, with rectifier (ramp, ReLU) be common.

The overall network is a combination of function composition and matrix multiplication:

.. math:: g(x) := f^L(W^Lf^{L-1}(W^{L-1}...f^1(W^1x)...))

Loss function:

.. math:: C(y_i, g(x_i))

