Stochastic Gradient Descent
==============================

Both statistical estimation and machine learning consider the problem of minimizing an objective function that has the form of a sum:

.. math:: Q(w) = \frac{1}{n}\sum_{i=0}^{n}Q_{i}(w)

where the parameter w that minimizes Q(w) is to be estimated. Each summand function :math:`Q_i` is typically associated with the i-th observation in the data set (used for training).

In classical statistics, sum-minimization problems arise in least squares in maximum-likelihood estimation (for independent observations). The general class of estimators that arise as minimizers of sums are called M-estimators. However, in statistics, it has been long recognized that requiring even local minimization is too restrictive for some problems of maximum-likelihood estimation. Therefore, contemporary statistical theorists often consider stationary points of the likelihood function (or zeros of its derivative). 

The sum-minimization problem also arises for empirical risk minimization. In this case, :math:`Q_i(w)` is the value of the loss function at *i*-th example, and :math:`Q(w)` is the empirical risk.

When used to minimize the above function, a standard (or "batch") gradient descent method would perform the following iterations:

.. math:: w := w - {\eta} {\nabla}Q(w) = w - \frac{\eta}{n}\sum_{i=1}^{n}{\nabla}Q_i(w)

where :math:`\eta` is the learning rate.

In many cases, the summand functions have a simple form that enables inexpensive evaluations of the sum-function and the sum gradient. For example, in statistics, one-parameter exponential families alow economical function-evaluations and gradient-evaluations.

However, in other cases, evaluating the sum-gradient may require expensive evaluations of the gradients from all summand functions. When the training set is enormous and no simple formulas exist, evaluating the sums of gradients becomes very expensive, because evaluating the gradient requires evaluating all the summand functions' gradients. To economize on the computational cost at every iteration, stochastic gradient descent samples a subset of summand functions at every step. This is very effective in the case of large-scale machine learning problems.

Iterative learning
---------------------

In stochastic (or "on-line") gradient descent, the true gradient of :math:`Q(w)` is approximated by a gradient at a single example:

.. math:: w := w - \eta\nabla{Q}_i(w).

As the algorithm sweeps through the training set, it performs the above update for each training example. Several passes can be make over the training set until the algorithm converges. If this is done, the data can be shuffled for each pass to prevent cycles. Typical implementations may use an adaptive learning rate so that the algorithm converges.

choose an initial vector of parameters w and learning rate :math:`eta`.
Repeat until an approximate minimum is obtained:
	* Randomly shuffle examples in the training set.
	* for i = 1,2,...,n,do:
		:math:`w:=w-{\eta}{\nabla}Q_i(w)`

A compromise between computing the true gradient and the gradient at a single example is to compute the gradient against more than one training example (called a "mini-batch") at each step. This can perform significantly better than "true" stochastic gradient descent described, because the code can make use of vectorization libraries rather than computing each step separately. It may also result in smoother convergence, as the gradient computed at each step is averaged over more training examples.

The convergence of stochastic gradient descent has been analyzed using the theories of convex minimization and of stochastic approximation. Briefly, when the learning rates ::math:`\eta` decrease with an appropriate rate, and subject to relatively mild assumptions, stochastic gradient descent converges almost surely to a global minimum when the objective function is convex or pseudoconvex, and otherwise converges almost surely to a local minimum. This is in fact a consequence of the Robbins-Siegmund theorem.

Example
--------

Let's suppose we want to fit a straight line :math:`\hat{y} = w_1 + w_2x` to a training set with observations :math:`(x_1,x_2,...,x_n)` and corresponding estimated responses :math:`\hat{y}_1,\hat{y}_2,...,\hat{y}_n` using least squares. The objective function to be minimized is:

.. math:: Q(w) = \sum_{i=1}^{n}Q_i(w) = \sum_{i=1}^{n}(\hat{y}_i - y_i)^2 = \sum_{i=1}^{n}(w_1 + w_2x_i - y_i)^2

The weight updated following the SGD method:

.. math:: 

	\begin{bmatrix}
		w_1 \\
		w_2
	\end{bmatrix}
	:=
	\begin{bmatrix}
		w_1 \\
		w_2
	\end{bmatrix}
	- \eta
	\begin{bmatrix}
		\frac{\partial}{\partial{w_1}}(w_1 + w_2x_i - y_i)^2 \\
		\frac{\partial}{\partial{w_2}}(w_1 + w_2x_i - y_i)^2
	\end{bmatrix}
	=
	\begin{bmatrix}
		w_1 \\
		w_2
	\end{bmatrix}
	- \eta
	\begin{bmatrix}
		2(w_2 + w_2x_i - y_i) \\
		2x_i(w_1 + w_2x_i - y_i)
	\end{bmatrix}.

Note that in each iteration (update), only the gradient evaluated at a single point ::math:`x_i` instead of evaluating at the set of all samples.

Momentum
--------------

Further proposals include the momentum method. Stochastic gradient descent with momentum remembers the update ::math:`\Delta{w}` at each iteration, and determines the next update as a linear combination of the gradient and the previous update:

.. math:: \Delta{w} := \alpha\Delta{w} - \eta\nabla{Q}_i(w)
.. math:: w := w + \Delta{w}

that leads to:

.. math:: w := w - \eta\nabla{Q}_i(w) + \alpha\Delta{w}

where the parameter :math:`w` which minimizes :math:`Q(w)` is to be estimated, :math:`\eta` is the learning rate and :math:`\alpha` is an exponential decay factor between 0 and 1 that determines the relative contibution of the current gradient and earlier gradients to the weight change.

The name momentum stems from an analogy to mementum in physics: the weight vector :math:`w`, thought of as a particle traveling through parameter space, incurs acceleration from the gradient of the loss ("force"). Unlike in classical stochastic gradient descent, it tends to keep traveling in the same direction, preventing oscillations. Momentum has been used successfully by computer scientists in the training of artificial neural networks for several decades.

AdaGrad
--------------
AdaGrad(for adaptive gradient algorithm) is a modified stochastic gradient descent algorithm with per-parameter learning rate.

.. math:: G = \sum_{\tau=1}^{t}g_{\tau}g_{\tau}^T


RMSProp
--------------
RMSProp (for Root Mean Square Propagation) is also a method in which the learning rate is adapted for each of the parameters. The idea is to divide the learning rate for a weight by a running average of the magnitudes of recent gradients for that weight.


Adam
--------------
Adam (Adaptive Moment Estimation) is an update to the RMSProp optimizer. In this optimization algorithm, running averages of both the gradients and the second moments of the gradients are used. Given parameters :math:`w^{(t)}` and a loss function :math:`L^{t}`, where t indexes the current training iteration (indexed at 0), Adam's parameter update is given by:

.. math:: m_w^{(t+1)} \leftarrow \beta_1m_w^{(t)} + (1 - \beta_1)\nabla_wL^{(t)}

.. math:: v_w^{(t+1)} \leftarrow \beta_2v_w^{(t)} + (1 - \beta_2)(\nabla_wL^{(t)})^2

.. math:: \hat{m}_w = \frac{m_w^{t+1}}{1-\beta_1^{t+1}}

.. math:: \hat{v}_w = \frac{v_w^{t+1}}{1-\beta_2^{t+1}}

.. math:: w^{t+1} \leftarrow w^t - \eta\frac{\hat{m}_w}{\sqrt{\hat{v}_w} + \epsilon}