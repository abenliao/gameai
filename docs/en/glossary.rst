..  _glossary:

Glossary
=========

.. glossary::

    FSM
        finite state machine

    NPC
        non-player character

    Automaton
        A machine or control mechanism designed to follow a predetermined sequence of operations automatically or respond to encoded instructions

