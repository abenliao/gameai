=====================
CUDA
=====================

CUDA (Compute Unified Device Architecture) is a parallel computing platform and application programming interface (API) created by Nvidia. [#]_

The CUDA API works with C/C++ and Python. 

Numba [#]_ supports CUDA GPU programming by directly compiling a restricted subset of Python code into CUDA kernels and device functions. 
Kernels written in Numba may access the NumPy arrays, which are transferred between the CPU and the GPU automatically.

.. toctree::
	:maxdepth: 2

	cuda/thrust

.. rubric:: Footnotes

.. [#] `Nvidia CUDA Home Page <https://developer.nvidia.com/cuda-zone>`_

.. [#] `Numba Home Page <http://numba.pydata.org>`_