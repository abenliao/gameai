Thrust
========

Thrust is a C++ template library for CUDA based on the Standard Template Library (STL).

https://docs.nvidia.com/cuda/thrust/index.html