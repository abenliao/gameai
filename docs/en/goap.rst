================================
GOAP
================================

Goal-oriented action planning [#]_ is a STRIPS-like planning algorithm designed to implement a flexible NPC actions control system.

.. code-block:: c

    #include "goap.h"
    #include "astar.h"

    actionplanner_t ap;
    goap_actionplanner_clear( &ap ); // initializes action planner

    // describe repertoire of actions
    goap_set_pre( &ap, "scout", "armedwithgun", true );
    goap_set_pst( &ap, "scout", "enemyvisible", true );

    goap_set_pre( &ap, "approach", "enemyvisible", true );
    goap_set_pst( &ap, "approach", "nearenemy", true );

    goap_set_pre( &ap, "aim", "enemyvisible", true );
    goap_set_pre( &ap, "aim", "weaponloaded", true );
    goap_set_pst( &ap, "aim", "enemylinedup", true );

    goap_set_pre( &ap, "shoot", "enemylinedup", true );
    goap_set_pst( &ap, "shoot", "enemyalive", false );

    goap_set_pre( &ap, "load", "armedwithgun", true );
    goap_set_pst( &ap, "load", "weaponloaded", true );

    goap_set_pre( &ap, "detonatebomb", "armedwithbomb", true );
    goap_set_pre( &ap, "detonatebomb", "nearenemy", true );
    goap_set_pst( &ap, "detonatebomb", "alive", false );
    goap_set_pst( &ap, "detonatebomb", "enemyalive", false );

    goap_set_pre( &ap, "flee", "enemyvisible", true );
    goap_set_pst( &ap, "flee", "nearenemy", false );

    // describe current world state.
    worldstate_t fr; 
    goap_worldstate_clear( &fr );
    goap_worldstate_set( &ap, &fr, "enemyvisible", false );
    goap_worldstate_set( &ap, &fr, "armedwithgun", true );
    goap_worldstate_set( &ap, &fr, "weaponloaded", false );
    goap_worldstate_set( &ap, &fr, "enemylinedup", false );
    goap_worldstate_set( &ap, &fr, "enemyalive", true );
    goap_worldstate_set( &ap, &fr, "armedwithbomb", true );
    goap_worldstate_set( &ap, &fr, "nearenemy", false );
    goap_worldstate_set( &ap, &fr, "alive", true );

    // describe desired world state.
    worldstate_t goal;
    goap_worldstate_clear( &goal );
    goap_worldstate_set( &ap, &goal, "enemyalive", false );
    //goap_worldstate_set( &ap, &goal, "alive", true ); // add this to avoid suicide actions in the plan.

    worldstate_t states[16];
    const char* plan[16]; // The planner will return the action plan in this array.
    int plansz=16; // Size of our return buffers.
    const int plancost = astar_plan( &ap, fr, goal, plan, states, &plansz );

.. rubric:: Footnotes

.. [#] http://alumni.media.mit.edu/~jorkin/goap.html