Preface
=========

This article aims to demystify the AI technologies commonly used in video games from primitive ages to the present.

It consists of several different topics. 

#. Automaton
#. Path-finding
#. Parallel Computing
#. Machine Learning
#. Reinforcement Learning
#. Autonomous Contents Generation
#. Anti-Cheating
#. Accurate, personalized promotions
  

The game
----------

There are various games, and we will focus on the kind with 2D or 3D scenes in which players or non-player characters can walk around and play. The technologies used to drive and control the characters behaving autonomously in this kind of game are what we are going to discuss here. RTS, MOBA, RPG, FPS, and TPS are the most typical games that belong to this type.

