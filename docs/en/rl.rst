Reinforcement Learning
================================================

Reinforcement Learning is the study of decision-making over time with consequences. The field has developed systems to make decisions in complex environments based on external and possibly delayed feedback.

.. figure:: ../images/rl.drawio.png
    :name: fig_rl

    Reinforcement Learning

**Action Spaces**

Action Spaces are classified into two different types: discrete action spaces and continuous action spaces.

**Polices**

A policy is a rule used by an agent to decide what actions should take.

**Deterministic Polices**

**Stochastic Polices**
 


Deep Reinforcement Learning
--------------------------------

.. admonition:: |:bulb:| TIPS |:question:|

    Classical Reinforcement Learning is not capable of solving the problem of complex systems.


.. figure:: ../images/drl.drawio.png
    :alt: Deep Reinforcement Learning
    :align: center
    :name: fig_drl

    Deep Reinforcement Learning

As depicted in :figure:numref:`fig_drl`, we need a

