Auxiliary Classifier GANs
============================

Auxiliary Classifier GANs [#]_ was introduced in the paper "Conditional Image Synthesis with Auxiliary Classifier GANs" by Augustus Odena, etc.

There is an open-source implementation of AC-GAN in Github. [#]_


.. rubric:: Footnotes

.. [#] https://arxiv.org/abs/1610.09585

.. [#] https://github.com/buriburisuri/ac-gan