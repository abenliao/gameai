Turing machine
===============================

A Turing machine is a mathematical model of computation that defines an abstract machine that manipulates symbols on a strip of tape according to a table of rules. 

The Turing machine was invented in 1936 by Alan Turing.

A Turing machine is a general example of a CPU that controls all data manipulation done by a computer.