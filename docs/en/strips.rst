Stanford Reseach Institute Problem Solver
================================================================

The Stanford Research Institute Problem Solver, known by its acronym STRIPS is used to refer to the formal language of the inputs of a planner to solve automated planning problem.

A STRIPS instance consists:
    * An initial state;
    * The specification of the goal states.
    * A set of actions. For each action, the following are included:
        * preconditions (what must be established before the action is performed);
        * posconditions (what is established after the the action is performed).

Mathematically, a STRIPS instance is quadruple <P, O, I G>, in which each component has the following meaning:

    #. P is a set of conditions (i.e., propositional variables);
    #. O is a set of operations.
    #. I is the initial state;
    #. G is the specification of the goal state;

A plan for such a planning instance is a sequence of operations that can be executed from the initial state and that leads to a goal state.

Formally, a state is a set of conditions: a state is represented by the set of conditions that are true in it. Transitions between states are modeled by a transition function, which is a function mapping states into new states that result from the execution of actions. Since states are represented by sets of conditions, the transition function relative to the STRIPS instance <P, O, I, G> is a function:

.. math:: succ: 2^P \times O \rightarrow 2^P
    :label: eqstr

where :math:`2^P` is the set of all subsets of P, and is therefore the set of all possible states. :eq:`eqstr`