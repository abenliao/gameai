PLANNING
========

Planning is the process of thinking about the activities required to achieve the desired goal. It involves the creation and maintenance of a plan, such as psychological aspects that need conceptual skills. It touches at least the following faces:

* Goal Oriented
* Pervasive
* Continuous Activity
* Intellectual process
* Futuristic
* Decision making
* Managerial Function