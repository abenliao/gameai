Navmesh
========

Navigation mesh is used to accelerate the path-finding process to walk on a 3D walking surface.

Mikko Mononen [#]_ implemented a set of widely used navigation mesh tools called recast detour [#]_, integrated into the famous Unreal and Unity game engine.


.. [#] https://github.com/memononen

.. [#] https://github.com/recastnavigation/recastnavigation