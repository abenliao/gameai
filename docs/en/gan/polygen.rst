PolyGen
================================

PolyGen: An Autoregressive Generative Model of 3D Meshes.

PolyGen takes a rather unique approach to the model generation task by representing a 3D model as a strickly ordered sequence of vertices and faces, instead of images, voxels, or point clouds. This strict ordering enables them to apply an attention-based sequence modeling approach towards generating 3D meshes, much like the BIRT or GPT models do for text.

The overall objective of PolyGen is two-fold: first generate a plausible set of vertices for a 3D model (perhaps conditioned by an image, voxels, or class label), then generate a series of faces, one by one, that connect vertices together and provide a plausible surface for this model. The combined model expresses a distribution over messhes p(M) as a joint distribution between two models: a vertex model p(V) to represent vertices and a face model p(F/V) to represent faces conditioned on vertices.

.. math:: p(M) = p(V,F)
