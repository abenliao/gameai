============================
Hierarchical Task Network
============================

A hierarchical task network (HTN) planning is an approach to automated planning in which the dependency among actions can be given in the form of hierarchically structured networks.

There are three kinds of task:

#. primitive tasks
#. compound tasks
#. goal tasks

A solution to an HTN problem is an executable sequence of primitive tasks that can be obtained from the initial task network by decomposing compound tasks into their set of simpler tasks and by inserting ordering constraints.

Constraints among tasks are expressed in the form of networks, called (hierarchical) task networks. A task network is a set of tasks and constraints among them. Such a network can be used as the precondition for another compound or goal task to be feasible. This way, one can express that a given task is feasible only if a set of other actions (those motioned in the network) are done, and they are done in such a way that the constrains among them (speicified by the network) are satisfied. One particular formalism for representing hierarchical task networks that has ben fairly widely used is TAEMS.

TAEMS
------

Task Analysis, Environment Modeling, and Simulation (TAEMS) is a problem domain independent modeling language used to describe the task structures and the problem-solving activities of intelligent agents in a multi-agent environment.

The intelligent agent operates in environments where:

* responses by specific deadlines may be required
* the information required for the optimal performance of a computational task may not be available
* the results of the computations of multiple agents to interdependent subproblems may need to be aggregated together in order to solve a high-level goal
* an agent may be contributing concurrently to be the solution of multiple goals.