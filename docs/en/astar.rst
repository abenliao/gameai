A* algorithm
=============

A* algorithm is the most widely used method to solve the path-finding problem in video games.

A* achieves optimality and completeness, two valuable properties of search algorithms.

.. note::
    When a search algorithm has the property of optimality, it is guaranteed to find the best possible solution. When a search algorithm has the property of completeness, it means that if a solution to a given problem exists, the algorithm is guaranteed to find it.

.. glossary::

    Node
        (also called State) All potential position or stops with a unique identification
    
    Transition
        The act of moving between states or nodes

    Starting Node
        Whereto start searching

    Goal Node
        The target to stop searching

    Search Space 
        A collection of nodes, like all board positions of a board game 
        
    Cost
        a numerical value (say distance, time, or financial expense) for the path from a node to another node

    g(n)
        represent the exact cost of the path from the starting node to any node n

    h(n)
        represent the heuristic estimated cost from node n to the goal node

    f(n)
        the lowest cost in the neighboring node n
        
        .. math:: f(n) = g(n) + h(n)
            :label: eq_astar_fn

    
