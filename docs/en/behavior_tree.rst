Behavior Tree
================================

Behavior Tree plays a dominant role in current game AI systems.



Unreal Engine Behavior Tree
--------------------------------

System Structure
^^^^^^^^^^^^^^^^

.. figure:: ../images/ue4bt.drawio.png
    :name: ue4bt

    Unreal Engine Behavior Tree Structure


Behavior Tree components
^^^^^^^^^^^^^^^^^^^^^^^^

Five main components make up a behavior tree:

* Root
* Composite
* Service
* Decorator
* Task
  
**Root**

* The starting execution node for the behavior tree.
* Every behavior tree has one.
* Can not attach Decorators or services to it.

**Composite**

* Nodes define the Root of a branch and define the base rules for how that branch is executed.
* Sequence, Selector, Simple Parallel

*Sequence*

Sequence Nodes execute their children from left to right, and will stop executing the rest children when any one of their children fails.

*Selector*

Selector Nodes execute their children from left to right, and will stop executing the rest of its children when one of their children succeeds and the selector succeeds.

*Simple Parallel*

Simple Parallel nodes have only two children: one which must be a single Task node, and the other of which must can be a complete sub-tree running until the single task is finished.

**Service**

A service is attached to Composite Nodes and executes at its defined frequency to make checks and update the Blackboard variables. It takes the place of the traditional Parallel node.

**Decorator**

Decorators, also known as conditionals, are attached to a node and decide on the executing of a branch or a single node.

**Task**

Tasks are tree leaves that take the actual actions.

**Blackboard**

A blackboard is a collection of variables on which decision-making depends, can be used by a simple AI pawn, and shared by a squad.
A Blackboard can cache events, calculations, intermediate data to facilitate data sharing and accelerate decision-making and action execution.

.. attention::
    
    * Don't clutter the Blackboard with lots of super-specific-case data.
    * If only one node needs to know something, it can potentially fetch the value itself rather than adding every bit of tree.
    * If data are failed to be copied to the Blackboard, it may cause a debugging nightmare.
    * If data are updated or copied in a high frequency, it could cause bad performance.


Unreal Engine documentation brings an excellent tutorial [#]_. I have created a boilerplate project following this tutorial, and it's available in the GitLab repository. [#]_

.. rubric:: Footnotes

.. [#] https://docs.unrealengine.com/4.26/en-US/InteractiveExperiences/ArtificialIntelligence/BehaviorTrees/BehaviorTreeQuickStart/

.. [#] https://gitlab.com/abenliao/bt_ue