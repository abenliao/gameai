Word2Vec
========

A word embedding is a learned representation for text where words that have the same meaning have a similar representation. 

*"One of the benefits of using dense and low-dimensional vectors is computational: the majority of neural network toolkits do not play well with very high-demensional, sparse vectors. ... The main benifit of the dense representations is generalization power: if we believe some features may provide similar clues, it is worthwhile to provide a representation that is able to capture these similarities."*

Page 92, Neural Network Methods in Natural Language Processing, 2017.

Word embeddings are in fact a class of techniques where individual words are represented as real-value vectors in a predefined vector space. Each word is mapped to one vector and the vector values are learned in a way that resembles a  neural network, and hence the technique is often lumped into the field of deep learning. 

Key to the approach is the idea of using a dense distributed representation for each word.

Each word is represented by a real-value vector, often tens or hundreds of dimensions. This is contrasted to the thousands or millions of dimensions required to sparse word representation, such as one-hot encoding.

The distributed representation is learned based on the usage of words. This allows words that used in similar ways to result in having similar representations, naturally capturing their meaning. This can be contrasted with the crisp but fragile representation in a bag of words model where, unless explicitly managed, different words have different representations, regardless of how they are used.

There is deeper linguistic theory behind the approach, namely the "distributional hypothesis" by Zellig Harris that could be summarized as: words that have similar context will have similar meanings. For more depth see Harris's 1956 paper "Distributional structure".

This notion of letting the usage of the word define its meaning can be summarized by an often repeated quip by John Firth:

"You should know a word by the company it keeps"

 Page 11, “A synopsis of linguistic theory 1930-1955“, in Studies in Linguistic Analysis 1930-1955, 1962.


 World Embedding Algorithms
 --------------------------------

 World embedding methods learn a real-value vector representation for a predefined fixed sized vocabulary from a corpus of text.

 The learning process is either joint with the neural network model on some task, such as document classification, or is an unsupervised process, using document statistics.

 This section reviews three techniques that can be used to learn a word embedding from text data.

1. Embedding Layer

An embedding layer, for lack of a better name, is a word embedding that is learned jointly with a neural network model on a specific natrual language processing task, such as language modeling or document classification.

It requires that document text be cleaned and prepared such that each word is on-hot encoded. The size of the vector space is specified as part of the model, such as 50, 100, or 300 dimensions. The vectors are initialized with small rondom numbers. The embedding layer is used on the front end of a neural network and is fit in a supervised way using the backpropagation algorithm.

The one-hot encoded words are mapped to the word vectors. If a multilayer Perceptron model is used, then the word vectors are concatenated before being fed as input to the model. If a recurrent neural network is used, then each word may be taken as one input in a sequence.

This approach of learning an embedding layer requires a lot of training data and can be slow, but will learn an embedding both targeted to the specific text data and the NLP task.

2. Word2Vec
   
Word2Vec is a statistic method for efficiently learning a standalone word embedding from a text corpus.

It was developed by Tomas Mikolov, et al. at Google in 2013 as a response to make the neural-network-based training of the embedding more efficient and since then has become the de facto standard for developing pre-trained word embedding.

Additionally, the work involved analysis of the learned vectors and the exploration of vector math on the representations of words. For example, the subtracting the "man-ness" from "King" and adding "women-ness" results in the words "Queen", capturing the analogy "king is to queen as man is to woman".

Two different learning models were introduced that can be used as part of the word2vec approach to learn the word embedding; they are:

* Continuous Bag-of Words, or CBOW model
* Continuous Skip-Gram Model 

The CBOW model learns the embedding by predicting the current word based on its context. The continuous skip-gram model learns by predicting the surrounding words given a current word.

Both models are focused on learning about words given their local usage context, where the context is defined by a window of neighboring words. This window is a configurable parameter of the model.

The key benifit of the approach is that high-quality word embeddings can be learned efficiently (low space and time complexity), allowing larger embeddings to be learned (more dimensions) from much larger corpora of text (billions of words).

3. GloVe
   
The Global Vectors for Word Representation, or GloVe,  algorithm is an extention to the word2vec method for efficiciently learning word vectors, developed by Pennington, et al. at Standford.

Classical vector space model representations of words were developed using matrix fatorization techniques such as Latent Semantic Analysis (LSA) that do a good job of using global text statisitcs but are not as good as the learned methods like word2vec capturing meaning and demonstrating it on tasks like calculating analogies (e.g. the King and Queen example above).

GloVe is an approach to marry both the global statistics of matrix fatorization techniques like LSA with the local context-based learning in word2vec.

Rather than using a window to define local context, GloVe constructs an explicit word-context or word co-occurrence matrix using statistics across the whole text corpus. The reuslt is a learning model that may result in generally better word embeddings.

GloVe is an unsupervised learning algorithm for obtaining vector representations for words. Training is performed on aggregated global word-word co-occurrence statistics from a corpus, and the resulting representations showcase interesting linear substructures of the word vector space.

Highlights 

1. The Nearest neighbors
2. Linear substructures
   
Training
--------------

The GloVe model is trained on the non-zero entries of a global word-word co-occurrence matrix, which tabulates how frequently words co-occurrence with one another in a given corpus. Populating this matrix requires a single pass through the entire corpus to collect the statistics. For large corpora, this pass can be computationally expensive, but it is a one-time up-front cost. Subsequent training iterations are much faster because the number of non-zero matrix entries is typically much smaller than the total number of words in the corpus.

GloVe is essentially a log-bilinear model with a weighted least-squares objective. The main intuition underlying the model is the simple observation that ratios of word-word co-occurrence probobilities have the potential for encoding some form of meaning. 

The training objective of GloVe is to learn word vectors such that their dot product equals the logarithm of the words' probability of co-occurrence. Owing to the fact that the logarithm of a ratio equals the difference of logarithms, this objective associates (the logarithm of) ratios of co-occurrence probabilities with vector differences in the word vector space. Because these ratios can encode some form of meaning, this information gets encoded as vector differences as well. For this reason, the resulting word vectors perform very well on word analogy tasks, such as those examined in the word2vec package.