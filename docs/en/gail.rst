Generative Adversarial Imitation Learning (GAIL)
==========================================================

Generative Adversarial Imitation Learning (GAIL) [#]_ uses expert trajectories to recover a cost function and then learn a policy.

Learning a cost function from expert demonstrations is called Inverse Reinforcement Learning (IRL). The connection between GAIL and Generative Adversarial Networks (GANs) is that it uses a discriminator that tries to separate expert trajectory from trajectories of the learned policy, which has the role of the generator here.


.. rubric:: Footnotes

.. [#] https://arxiv.org/abs/1606.03476