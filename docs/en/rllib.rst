RLlib
================================================================

RLlib [#]_ is an open-source library for reinforcement learning that offers both high scalability and a unified API for a variety of applications. RLlib natively supports TensorFlow, TensorFlow Eager [#]_, and PyTorch, but most of its internals are framework agnostic.

.. code-block:: python

    import ray
    from ray import tune
    from ray.rllib.agents.ppo import PPOTrainer

    if not ray.is_initialized():
        ray.init(num_gpus=1) # Skip or set to ignore if already called

    tune.run(PPOTrainer, config={
            "env": "CartPole-v0",
            "framework": "tf2",
            "num_gpus": 1,
            "num_workers": 4,
        },
    )


Polices
--------------------------------

Polices are a core concept in RLlib. In a nutshell, policies are Python classes that define how an agent acts in an environment. Rollout workers query the policy to determine agent actions. In a gym environment, there is a single agent and policy. In vector envs, policy inference is for multiple agents at once, and in multi-agent, there may be multiple policies, each controlling one or more agents:

Polices can be implemented using any framework. Howerver, for TensorFlow and PyTorch, RLlib has build_tf_policy and build_torch_policy helper functions that let you define a trainable policy with a functional-style API, for example:

.. code-block:: python
    
    def policy_gradient_loss(policy, model, dist_class, train_batch):
        logits, _ = model.from_batch(train_batch)
        action_dist = dist_class(logits, model)
        return -tf.reduce_mean(
            action_dist.logp(train_batch["actions"]) * train_batch["rewords"]),
    
    MyTFPolicy = build_tf_policy(
        name="MyTFPolicy",
        loss_fn=policy_gradient_loss)


Sample Batches
----------------

Whether runing in a single process or large cluster, all data interchange in RLlib is in the form of sample batches. Sample batches encode one or more fragments of a trajectory. Typically, RLlib collects batches of size roolout_fragment_lenght from rollout workers, and concatenates one or more of these batches in to a batch of size *train_batch_size* that is the input to SGD.

A typical sample batch looks something like the following the follwing when summarized. Since all values are kept in arrays, this allows for efficient encoding and transmission across the network:

.. code-block:: python

    {'action_logp': np.ndarray((200,), dtype=float32, min=-0.701, max=-0.685, mean=-0.694),
    'actions': np.ndarray((200,), dtype=int64, min=0.0, max=1.0, mean=0.495),
    'dones': np.ndarray((200,), dtype=bool, min=0.0, max=1.0, mean=0.055),
    'infos': np.ndarray((200,), dtype=object, head={}),
    'new_obs': np.ndarray((200, 4), dtype=float32, min=-2.46, max=2.259, mean=0.018),
    'obs': np.ndarray((200, 4), dtype=float32, min=-2.46, max=2.259, mean=0.016),
    'rewards': np.ndarray((200,), dtype=float32, min=1.0, max=1.0, mean=1.0),
    't': np.ndarray((200,), dtype=int64, min=0.0, max=34.0, mean=9.14)}

In multi-agent mode, sample batches are collected separately for each individual policy.

Training
--------------

Polices each define a learn_on_batch() method that improves the policy given a sample batch of input. For TF and Torch policies, this is implemented using a loss function that takes as input sample batch tensors and outputs a scalar loss. Here are a few example loss functions:

    * Simple policy gradient loss
    * Simple Q-function loss
    * Importance-weighted APPO surrogate loss.

RLlib Trainer classes coordinate the distributed workflow of running rollouts and optimizing policies. They do this leveraging Ray parallel iterators to implement the desired computation pattern. The following figure shows synchronous sampling, the simplest of these patterns:

.. figure:: ../images/a2c-arch.svg
    :name: sync_sampling

    Synchronous Sampling (e.g., A2C, PG, PPO)

RLlib uses Ray actors to scale training from a single core to many thouthands of cores in a cluster. You can configure the parallelism used for training by changing the num_workers parameter.

Beyond environments defined in Python, RLlib supports batch training on offline datasets.

Customization
----------------
RLlib provides ways to customize almost all aspects of training, including neural network models, action distributions, policy definitions: the environment, and the Sample collection process.

.. figure:: ../images/rllib-components.svg
    :name: rllib_coms

    RLlib components

The following code gives an example of how to use RLlib with a custom environment and model.

.. code-block:: python

    """Example of a custom gym environment and model. Run this for a demo.
    This example shows:
    - using a custom environment
    - using a custom model
    - using Tune for grid search
    You can visualize experiment results in ~/ray_results using TensorBoard.
    """
    import argparse
    import gym
    from gym.spaces import Discrete, Box
    import numpy as np
    import os
    import random

    import ray
    from ray import tune
    from ray.tune import grid_search
    from ray.rllib.env.env_context import EnvContext
    from ray.rllib.models import ModelCatalog
    from ray.rllib.models.tf.tf_modelv2 import TFModelV2
    from ray.rllib.models.tf.fcnet import FullyConnectedNetwork
    from ray.rllib.models.torch.torch_modelv2 import TorchModelV2
    from ray.rllib.models.torch.fcnet import FullyConnectedNetwork as TorchFC
    from ray.rllib.utils.framework import try_import_tf, try_import_torch
    from ray.rllib.utils.test_utils import check_learning_achieved

    tf1, tf, tfv = try_import_tf()
    torch, nn = try_import_torch()

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--run",
        type=str,
        default="PPO",
        help="The RLlib-registered algorithm to use.")
    parser.add_argument(
        "--framework",
        choices=["tf", "tf2", "tfe", "torch"],
        default="tf",
        help="The DL framework specifier.")
    parser.add_argument(
        "--as-test",
        action="store_true",
        help="Whether this script should be run as a test: --stop-reward must "
        "be achieved within --stop-timesteps AND --stop-iters.")
    parser.add_argument(
        "--stop-iters",
        type=int,
        default=50,
        help="Number of iterations to train.")
    parser.add_argument(
        "--stop-timesteps",
        type=int,
        default=100000,
        help="Number of timesteps to train.")
    parser.add_argument(
        "--stop-reward",
        type=float,
        default=0.1,
        help="Reward at which we stop training.")


    class SimpleCorridor(gym.Env):
        """Example of a custom env in which you have to walk down a corridor.
        You can configure the length of the corridor via the env config."""

        def __init__(self, config: EnvContext):
            self.end_pos = config["corridor_length"]
            self.cur_pos = 0
            self.action_space = Discrete(2)
            self.observation_space = Box(
                0.0, self.end_pos, shape=(1, ), dtype=np.float32)
            # Set the seed. This is only used for the final (reach goal) reward.
            self.seed(config.worker_index * config.num_workers)

        def reset(self):
            self.cur_pos = 0
            return [self.cur_pos]

        def step(self, action):
            assert action in [0, 1], action
            if action == 0 and self.cur_pos > 0:
                self.cur_pos -= 1
            elif action == 1:
                self.cur_pos += 1
            done = self.cur_pos >= self.end_pos
            # Produce a random reward when we reach the goal.
            return [self.cur_pos], \
                random.random() * 2 if done else -0.1, done, {}

        def seed(self, seed=None):
            random.seed(seed)


    class CustomModel(TFModelV2):
        """Example of a keras custom model that just delegates to an fc-net."""

        def __init__(self, obs_space, action_space, num_outputs, model_config,
                    name):
            super(CustomModel, self).__init__(obs_space, action_space, num_outputs,
                                            model_config, name)
            self.model = FullyConnectedNetwork(obs_space, action_space,
                                            num_outputs, model_config, name)

        def forward(self, input_dict, state, seq_lens):
            return self.model.forward(input_dict, state, seq_lens)

        def value_function(self):
            return self.model.value_function()


    class TorchCustomModel(TorchModelV2, nn.Module):
        """Example of a PyTorch custom model that just delegates to a fc-net."""

        def __init__(self, obs_space, action_space, num_outputs, model_config,
                    name):
            TorchModelV2.__init__(self, obs_space, action_space, num_outputs,
                                model_config, name)
            nn.Module.__init__(self)

            self.torch_sub_model = TorchFC(obs_space, action_space, num_outputs,
                                        model_config, name)

        def forward(self, input_dict, state, seq_lens):
            input_dict["obs"] = input_dict["obs"].float()
            fc_out, _ = self.torch_sub_model(input_dict, state, seq_lens)
            return fc_out, []

        def value_function(self):
            return torch.reshape(self.torch_sub_model.value_function(), [-1])


    if __name__ == "__main__":
        args = parser.parse_args()
        ray.init()

        # Can also register the env creator function explicitly with:
        # register_env("corridor", lambda config: SimpleCorridor(config))
        ModelCatalog.register_custom_model(
            "my_model", TorchCustomModel
            if args.framework == "torch" else CustomModel)

        config = {
            "env": SimpleCorridor,  # or "corridor" if registered above
            "env_config": {
                "corridor_length": 5,
            },
            # Use GPUs iff `RLLIB_NUM_GPUS` env var set to > 0.
            "num_gpus": int(os.environ.get("RLLIB_NUM_GPUS", "0")),
            "model": {
                "custom_model": "my_model",
                "vf_share_layers": True,
            },
            "lr": grid_search([1e-2, 1e-4, 1e-6]),  # try different lrs
            "num_workers": 1,  # parallelism
            "framework": args.framework,
        }

        stop = {
            "training_iteration": args.stop_iters,
            "timesteps_total": args.stop_timesteps,
            "episode_reward_mean": args.stop_reward,
        }

        results = tune.run(args.run, config=config, stop=stop)

        if args.as_test:
            check_learning_achieved(results, args.stop_reward)
        ray.shutdown()



The Trajectory View API
--------------------------------

The trajectory view API should solve two significant problems:

#. Make complex model support possible
#. allow for a faster (environment) sample collection and retrieval system.

The trajectory view API is a dictionary, mapping keys (str) to "view requirement" objects. The defined keys correspond to available keys in the input-Dicts (or SampleBatches) with which models are called. We also call these keys "views". The dictionary is defined in a model's constructor (see the self.view_requirements property of the ModelV2 class). In the default case, it contains only one entry (the "obs" key). The value is a View is a ViewRequirement object telling RLlib to not perform any "shifts" (time-wise) on the collected observations for this view.

.. code-block:: python

    self.view_requirements = {"obs" : ViewRequirement (shift=0)}



.. figure:: ../images/trajectory_view.drawio.png

Building software for a rapidly developing field such as RL is challenging, multi-agent especially so. This is in part due to the breath of techniques used to deal with the core issues that arise in multi-agent learning.


Training
--------------

Policies each define a learn_on_batch() method that provides the policy given a sample batch of of input. For TF and Torch polices, this is implemented using a loss function that takes as input sample batch tensors and outputs a scaler loss. Here are a few example loss function:
* Simple policy gradient loss
* Simple Q-function loss
* Importance-weighted APPO surrogate loss

RLlib Trainer classes coordinate the distributed workflow of running rollouts and optimizing policies. They do this by leveraging Ray parallel iterators to implement the desired computation pattern. 

RLlib uses Ray actors to scale training from a single core to many thousands of cores in a cluster. You can configure the parallelism used for training by changing the num_workers parameter. 

Customization
--------------

RLlib provides ways to customize almost all aspects of training, including neural network models, action distributions, policy definitions: the environment,and the sample collection process. 


Tune
--------------

Tune is a Python library for experiment execution and hyperparameter tuning at any scale. Core features:

* Launch a multi-node distributed hyperparameter sweep in less than 10 lines code.
* Supports any machine learning framework, including PyTorch,

.. rubric:: Footnotes

.. [#] https://docs.ray.io/en/master/rllib.html
.. [#] An imperative programming environment for TensorFlow without building graphs.