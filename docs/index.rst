=========
Applied game AI technologies
=========

The game
=========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   en/preface


Automaton
=========

.. toctree::
   :maxdepth: 2

   en/state_machine


Path Finding
=============

.. toctree::
   :maxdepth: 2

   en/dijkstra
   en/astar
   en/navmesh

.. include:: en/planning/index.rst

.. toctree::
   :maxdepth: 2
   
   en/strips
   en/behavior_tree
   en/eqs
   en/goap
   en/htn
   en/utility

Parallel Computing
===================

.. toctree::
   :maxdepth: 2

   en/cuda

Machine  Learning
==================

.. toctree::
   :maxdepth: 2

   en/dl/sgd
   en/dl/bp
   en/dlplatform
   en/rl
   en/rllib
   en/gail
   en/acgan
   en/attention
   en/gan/polygen   


Suffixes
=========

.. toctree::
   :maxdepth: 2

   en/glossary
